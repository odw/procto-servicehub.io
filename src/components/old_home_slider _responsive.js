/* 
1.Array of photos
2. Array of thumbs
SliderImgAndThumbsArray[[photo,thumb]] Recive in props
3.Responsive <900 10 thumbs 600-900 7 ,  600-450 4 , >450 3
4. Prop bool show thumbs
5. Animation Loop
6. Thumb click changes cur pos of slider
7.Local state cur pos =0;VisThumbCount=10
8.Cur pos . When its changes in state changes cur slede and cur thumb. onthumbclick onslidearrclick
9. -onthumbarrclick  
    if (cur first vis thumb + last vis thumb div(resp thumbs count) > SliderDataArray.length)  {vis thumbs count=(cur first vis thumb+resp thumbs count)-SliderDataArray.length}
    else {cur first vis thumb=cur first vis thumb}
10. if SliderDataArray.length<vis thumb count thumb arr click do nothing
11.For thums make 10 divs with ids 1 to 10 // nth child css after resp count make display none
12. Get first thum div with display none
*/

import React from 'react';

const sliderImgDir='/static/images/gallery/1/'
const imgUrlsNames=[
  '01-3124bdb512.jpg',
 '1-1fc1b98da0.jpg',
'1merc-fa86661dd7.jpg',
'5porsche-a649cf3912.jpg',
'a_bmw-edc770d177.jpg',
'a4-a973b502ea.jpg'
]
let thumbsCount=4;
const imgUrls=imgUrlsNames.map(val=>sliderImgDir+val);
//console.log(imgUrls);
const Arrow = ({ direction, clickFunction, glyph }) => (
	<div 
		className={ `slide-arrow-${direction}` } 
		onClick={ clickFunction }>
		{/*glyph */} 
	</div>
);
const ArrowThumb = ({ direction, clickFunction, glyph }) => (
	<div 
		className={ `slider-${direction}` } 
		onClick={ clickFunction }>
		{/*glyph */} 
	</div>
);

/*const ThumbImgItem = (imgId)=> {
	let className='set-img'+imgId;
	if (this.state.thumImgItemActive && imgid===1 ? 'set-img1 active': 'set-img1')
	return (<img className={className}  src="/static/media/01-3124bdb512.6699ab4a.jpg" onClick={this.addThumbImgItemActive(imgId)} />
)}; */


 class HomeSlider extends React.Component {



  
    constructor(props) {
      super();
      //let showThumbs=true;
       let {showThumbs=true} =props;
      this.state = { 
        currentImageIndex:0,
        //thumbsCount:1,
        startThumbIndex:0,
        //respThumbsCount:3, 
        showThumbs:showThumbs, 
        //height: window.innerHeight, 
       // width: window.innerWidth
      };
      console.log(this.state)
      //this.updateDimensions = this.updateDimensions.bind(this);
    }
    componentDidMount() {
      //console.log(this.state.height);
      // Additionally I could have just used an arrow function for the binding `this` to the component...
    //  window.addEventListener("resize", this.updateDimensions);
    }

/*
    updateDimensions() {
      let respThumbsCount=3 ;
      console.log(window.innerWidth)  ;
      if ( window.innerWidth>991.98) respThumbsCount=10 ;
      if (window.innerWidth>767.98 && window.innerWidth<=991.98) respThumbsCount=8 ;
      if (window.innerWidth>575.98 && window.innerWidth<=767.98) {respThumbsCount=6 ; }
      if (window.innerWidth<=575.98) this.respThumbsCount=3;
      this.setState({
         respThumbsCount: respThumbsCount ,
        height: window.innerHeight, 
        width: window.innerWidth
      });
    }
*/
    nextSliderImg() {}
    previousSliderImg() {}
    nextThumbs=()=> {
      let startThumbIndex=this.state.startThumbIndex;
      //let respThumbsCount=this.state.respThumbsCount;
      //if (respThumbsCount-imgUrls.length>=0) return; 
      //let thumbsCount=this.state.thumbsCount;
      if (startThumbIndex+respThumbsCount<imgUrls.length) {
        startThumbIndex=startThumbIndex+respThumbsCount;
        }
         else 
         {
          startThumbIndex=0;
      }
      if (imgUrls.length-startThumbIndex<respThumbsCount) {
        //thumbsCount=imgUrls.length%respThumbsCount;      
      } //else thumbsCount=this.respThumbsCount;
     // this.setState({
     ///   thumbsCount: thumbsCount
     // });

    }
    previousThumbs=()=> {
      /*startThumbIdex=this.state.startThumbIdex;
      respThumbsCount=this.state.respThumbsCount;
      //if ()
      //if (startThumbIdex=0) if imgUrls.length-
      //startThumbIndex=startThumbIndex-this.state.thumbsCount;
      //if (startThumbIndex<0) {startThumbIdex=imgUrls.length-this.state.thumbsCount;}
      if (imgUrls.length%respThumbsCount<respThumbsCount) {
        thumbsCount=imgUrls.length%respThumbsCount
      } else {thumbsCount=imgUrls.length-startThumbIdex};
      this.setState({
        thumbsCount: thumbsCount
      }); */
    }
     nextSlide =()=> {
      const lastIndex = imgUrls.length - 1;
      const { currentImageIndex } = this.state;
      const shouldResetIndex = currentImageIndex === lastIndex;
      const index =  shouldResetIndex ? 0 : currentImageIndex + 1;
  
      this.setState({
        currentImageIndex: index
      });
    }
    previousSlide =()=> {
      const lastIndex = imgUrls.length - 1;
      const { currentImageIndex } = this.state;
      const shouldResetIndex = currentImageIndex === 0;
      const index =  shouldResetIndex ? lastIndex : currentImageIndex - 1;
      
      this.setState({
        currentImageIndex: index
      });
    }

    render() {
      let  {showThumbs}=this.state;

      const renderShowThumbs = ()=>{
        //const reptiles = ["alligator", "snake", "lizard"];
        const stylesCenter = {  
          display: "flex",
          justifyContent:"center",        
          height:"102px",
          width:"75%",
          margin: "0 auto"
          };
          const styles = {  
            display: "flex",
            justifyContent:"space-between",        
            height:"102px",
            width:"75%",
            margin: "0 auto"
            };  
        if (this.state.showThumbs) 
        return (
       <div className="thumbs" style={styles}>   
       <img src={require('../images/arrow_left.svg')} alt="logo" className= ""/>      
        {/*<ArrowThumb direction="prev" clickFunction={ this.previousThumb }  />          
          //for(this.state.currentImageIndex+this.state.respThumbsCount)*/}
          <div style={stylesCenter}>
            {
          imgUrls. map((val) =>
           <div style={{maxHeight:'100%',height:'100%'}}>
            <img src={val} style={{maxWidth:'100%',maxHeight:'100%'}}/>
            </div>)}             
           {/* <ArrowThumb direction="next" clickFunction={ this.nextThumb }  /> */}
       </div> 
       <img src={require('../images/arrow_right.svg')} alt="logo" />  
       </div>    
       );
        }

     const SliderImg = ({ url }) => {
      const styles = {
      height:"auto",
      width:"75%"
      };
      const src=`${url}`;
      //console.log();
      return (
        <img className="image-slide" style={styles} src={src}></img>
      );
      }
      


      return (
        <div>
        <h3>
          Window width: {this.state.width} and height: {this.state.height} and rtc {this.state.respThumbsCount}
        
        </h3>
        {/*SliderImg( imgUrls[this.state.currentImageIndex])*/}
        <div className="carousel">
        <Arrow direction="left" clickFunction={ this.previousSlide } glyph="&#9664;" />
        <SliderImg url={ imgUrls[this.state.currentImageIndex] }/>
				<Arrow direction="right" clickFunction={ this.nextSlide } glyph="&#9654;" />
        
        </div>
        { renderShowThumbs()}
        
        </div>
        )
    }

    



    componentWillUnmount() {
      window.removeEventListener("resize", this.updateDimensions);
    }
  }

export default HomeSlider;