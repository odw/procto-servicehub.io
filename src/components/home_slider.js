/* 
1.Array of photos
2. Array of thumbs
SliderImgAndThumbsArray[[photo,thumb]] Recive in props
3.Responsive <900 10 thumbs 600-900 7 ,  600-450 4 , >450 3
4. Prop bool show thumbs
5. Animation Loop
6. Thumb click changes cur pos of slider
7.Local state cur pos =0;VisThumbCount=10
8.Cur pos . When its changes in state changes cur slede and cur thumb. onthumbclick onslidearrclick
9. -onthumbarrclick  
    if (cur first vis thumb + last vis thumb div(resp thumbs count) > SliderDataArray.length)  {vis thumbs count=(cur first vis thumb+resp thumbs count)-SliderDataArray.length}
    else {cur first vis thumb=cur first vis thumb}
10. if SliderDataArray.length<vis thumb count thumb arr click do nothing
11.For thums make 10 divs with ids 1 to 10 // nth child css after resp count make display none
12. Get first thum div with display none
*/

import React from "react"

const sliderImgDir = "/static/images/gallery/procto_main_slider/"
const imgUrlsNames = [
  "502.jpg",
  "504.jpg",
  "506.jpg",
  "521.jpg",
  "522.jpg",
  "532.jpg",
  "536.jpg",
  "537.jpg",
  "538.jpg",
  "539.jpg",
  "540.jpg",
]

const imgUrls = imgUrlsNames.map(val => sliderImgDir + val)
//console.log(imgUrls);
const Arrow = ({ direction, clickFunction, glyph }) => (
  <div className={`slide-arrow-${direction}`} onClick={clickFunction}>
    {/*glyph */}
  </div>
)
const ArrowThumb = ({ direction, clickFunction, glyph }) => (
  <div className={`slider-${direction}`} onClick={clickFunction}>
    {/*glyph */}
  </div>
)

/*const ThumbImgItem = (imgId)=> {
	let className='set-img'+imgId;
	if (this.state.thumImgItemActive && imgid===1 ? 'set-img1 active': 'set-img1')
	return (<img className={className}  src="/static/media/01-3124bdb512.6699ab4a.jpg" onClick={this.addThumbImgItemActive(imgId)} />
)}; */

class HomeSlider extends React.Component {
  maxThumbs = 4
  thumbsCount = this.maxThumbs

  constructor(props) {
    super()
    let { showThumbs = true } = props
    //console.log(showThumbs);
    this.state = {
      currentImageIndex: 0,
      activeThumbIndex: 0,
      startThumbIndex: 0,
      thumbsCount: 4,
      showThumbs: showThumbs,
      clickedThumb: false,
    }
  }

  componentDidMount() {}

  nextSliderImg() {}

  previousSliderImg() {}

  nextThumbs = () => {
    let startThumbIndex = this.state.startThumbIndex
    let thumbsCount = this.state.thumbsCount
    if (startThumbIndex === 0 && thumbsCount > imgUrls.length) {
      return
    }
    if (startThumbIndex + thumbsCount > imgUrls.length - 1) {
      startThumbIndex = 0
    } else {
      startThumbIndex = startThumbIndex + thumbsCount
    }
    if (startThumbIndex + thumbsCount > imgUrls.length - 1)
      thumbsCount = imgUrls.length - startThumbIndex
    else thumbsCount = this.maxThumbs
    this.setState({
      thumbsCount: thumbsCount,
      startThumbIndex: startThumbIndex,
    })
  }
  previousThumbs = () => {
    let startThumbIndex = this.state.startThumbIndex
    let thumbsCount = this.state.thumbsCount
    //console.log(startThumbIndex+" "+thumbsCount)
    if (startThumbIndex === 0 && thumbsCount > imgUrls.length) {
      return
    }
    if (startThumbIndex - thumbsCount >= 0) {
      thumbsCount = this.maxThumbs
      startThumbIndex = startThumbIndex - thumbsCount
    } else {
      if (imgUrls.length % this.maxThumbs !== 0) {
        startThumbIndex =
          imgUrls.length - 1 - Math.floor(imgUrls.length / this.maxThumbs)
        thumbsCount = imgUrls.length - startThumbIndex
      }
    }
    this.setState({
      thumbsCount: thumbsCount,
      startThumbIndex: startThumbIndex,
    })
  }
  nextSlide = () => {
    const lastIndex = imgUrls.length - 1
    const { currentImageIndex } = this.state
    const shouldResetIndex = currentImageIndex === lastIndex
    const index = shouldResetIndex ? 0 : currentImageIndex + 1

    this.setState({
      currentImageIndex: index,
    })
  }
  previousSlide = () => {
    const lastIndex = imgUrls.length - 1
    const { currentImageIndex } = this.state
    const shouldResetIndex = currentImageIndex === 0
    const index = shouldResetIndex ? lastIndex : currentImageIndex - 1

    this.setState({
      currentImageIndex: index,
    })
  }
  handleThumbClick = event => {
    //this.setState({clickedThumb: true});
    //console.log(event.target.getAttribute('data-key'));
    let ati = parseInt(event.target.getAttribute("data-key"))
    //console.log(ati)
    this.setState({
      activeThumbIndex: ati,
      currentImageIndex: ati,
    })
    // console.log("clicked thumb"+event.currentTarget.value);
  }
  handleThumbKeyDown = e => {
    //this.setState({clickedThumb: true});
    // console.log(e.target.console.log(e.target.getAttribute('data-key')));
    // console.log("clicked thumb");
  }

  render() {
    let { showThumbs } = this.state

    const renderShowThumbs = () => {
      //const reptiles = ["alligator", "snake", "lizard"];

      const stylesCenter = {
        display: "flex",
        justifyContent: "center",
        padding: "8px 0",

        margin: "0 auto",
      }
      const styles = {
        display: "flex",
        justifyContent: "space-between",

        width: "90%",
        margin: "0 auto",
      }
      //console.log(this.showThumbs)
      // console.log(this.state.thumbsCount+ " "+this.state.startThumbIndex)
      let s = this.state.startThumbIndex
      let t = this.state.thumbsCount
      let a = imgUrls.slice(s, s + t)
      let atin = this.state.activeThumbIndex
      //console.log(atin+"show thmb")
      //console.log(a)
      // let className = this.state.clickedThumb ? 'active' : '';
      let ca = []
      for (let i = 0; i <= imgUrls.length; i++) {
        i == atin ? (ca[i] = "active") : (ca[i] = "")
      }
      //console.log(ca);
      //const className = (this.state.startThumbIndex+index+index===this.state.activeThumbIndex)?'active':'';
      if (this.state.showThumbs)
        return (
          <div className="thumbs" style={styles}>
            <img
              src={require("../images/arrow_left.svg")}
              alt="logo"
              className=""
              onClick={this.previousThumbs}
            />
            {/*<ArrowThumb direction="prev" clickFunction={ this.previousThumb }  />          
          //for(this.state.currentImageIndex+this.state.respThumbsCount)*/}
            <div style={stylesCenter}>
              {a.map((val, index) => (
                // let c;
                //if (this.state.startThumbIndex+index+index===this.state.activeThumbIndex) {let c='active'} else {let c='';}
                <img
                  src={val}
                  className={ca[s + index]}
                  data-key={this.state.startThumbIndex + index}
                  style={{
                    maxWidth: "100%",
                    maxHeight: "100%",
                    margin: "0 4px",
                  }}
                  onClick={e => this.handleThumbClick(e)}
                />
              ))}
              {/* <ArrowThumb direction="next" clickFunction={ this.nextThumb }  /> */}
            </div>
            <img
              src={require("../images/arrow_right.svg")}
              alt="logo"
              onClick={this.nextThumbs}
            />
          </div>
        )
    }

    const SliderImg = ({ url }) => {
      const styles = {
        height: "100%",
        width: "auto",
      }
      const src = `${url}`
      //const src=require(url);
      //console.log();
      return <img className="image-slide" style={styles} src={src}></img>
    }

    return (
      <div>
        {/*SliderImg( imgUrls[this.state.currentImageIndex])*/}
        <div className="carousel">
          <Arrow
            direction="left"
            clickFunction={this.previousSlide}
            glyph="&#9664;"
          />
          <div className="slider-img">
            <SliderImg url={imgUrls[this.state.currentImageIndex]} />
          </div>
          <Arrow
            direction="right"
            clickFunction={this.nextSlide}
            glyph="&#9654;"
          />
        </div>
        {renderShowThumbs()}
      </div>
    )
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }
}

export default HomeSlider
