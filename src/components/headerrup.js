import React from 'react';


export default function HeaderRup () {
    return (
<div className={'headerrup'}>
    
<a href="tel:380633069848" className="button-blue-border" style={{display:"flex",flexDirection:"row"}}>
    <h3>Одесса, Водопроводная 10, +380633069848</h3>
    <span ></span>
    <img src={require('../images/phone-24px.svg')} alt="logo" />    
     <img src={require('../images/telegram_logo.svg')} alt="logo" /> 
    <img src={require('../images/viber_logo.svg')} alt="logo" />      
    </a>
</div>
    )
}