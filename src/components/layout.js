/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from 'react';
import PropTypes from 'prop-types';
import {useStaticQuery, graphql} from 'gatsby';
import HeaderL from './headerl';
import HeaderRUp from './headerrup';
import HeaderRD from './headerrd';
import Footer from './footer';
import HomeSlider from './home_slider';
import {Link} from 'gatsby';
//import Header from "./header"
//import "./layout.css"

const Layout = ({children}) => {
  /*const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)
 */
  return (
    <div className="container">
      <div className="wrapper">
        <HeaderL />
        <HeaderRUp />
        <HeaderRD />
        <div className="margin-bg3"></div>
        {/*<div className="main-home">{children}</div>*/}
        <div className="main-home">
          <HomeSlider showThumbs={true} />
          <div className="services-container">
            <div className="services-item">
              <Link to="/exhaust">
                <img
                  src={require('../../public/static/images/services_main_container/exhaust.jpg')}
                  style={{width: '100%'}}
                  alt="logo"
                />
              </Link>
              <div>Изготовление выхлопных систем</div>
            </div>
            <div className="services-item">
              <Link to="/chip_tuning">
                <img
                  src={require('../../public/static/images/services_main_container/revo.png')}
                  style={{width: '100%'}}
                  alt="logo"
                />
              </Link>
              <div>CHIP TUNING REVO</div>
            </div>
            <div className="services-item">
              <Link to="/maintenance">
                <img
                  src={require('../../public/static/images/services_main_container/service.jpg')}
                  style={{width: '100%'}}
                  alt="logo"
                />
              </Link>
              <div>
                Ремонт Обслуживание, диагностика, экспресс замена масел,
                фильтров, колодок
              </div>
            </div>
            <div className="services-item">
              <Link to="/car_painting">
                <img
                  src={require('../../public/static/images/services_main_container/painting_640x640.png')}
                  style={{width: '100%'}}
                  alt="logo"
                />
              </Link>
              <div>Покраска авто</div>
            </div>
          </div>
          <div
            className="main-article"
            style={{
              padding: '10px',
              backgroundColor: 'white',
              color: 'black',
              margin: '20px',
            }}
          >
            Главная <br /> <h3>Добро пожаловать на сайт PROCTO Service.</h3>{' '}
            <h4>О нас</h4>Мы одно из лучших тьюнинг СТО в Одессе.Работаем с 2014
            г. Специалиируемся на чип тьюнинге, выхлопных системах, авто из США,
            покраске, карбоне, подушках безопасности и сервисном обслуживаниии.
            Имеем большой опыт работы с VAG. Мы официальные представители
            компании REVO, которая являеться лучшим разработчиком чип тьюнинг
            прошивок в мире.
            <h4>Наши достижения</h4> Мы прошили более 150 авто, изготовили более
            100 выхлопных систем, привезли более 150 авто из США. Также мы
            являемся призерами по DRAG RACING в классах PRO AWD и STREET.
          </div>
          <div className="services-container">
            <div className="services-item">
              <Link to="/usa_car">
                <div>
                  <img
                    src={require('../../public/static/images/services_main_container/usa_car.jpg')}
                    style={{width: '100%'}}
                    alt="logo"
                  />
                </div>
              </Link>
              Авто из США
            </div>
            <div className="services-item">
              <Link to="/carbon">
                <div>
                  <img
                    src={require('../../public/static/images/services_main_container/carbon_640x640.png')}
                    style={{width: '100%'}}
                    alt="logo"
                  />
                </div>
              </Link>
              <div>Карбон</div>
            </div>
            <div className="services-item">
              <Link to="/airbag">
                <div>
                  <img
                    src={require('../../public/static/images/services_main_container/airbag.jpg')}
                    style={{width: '100%'}}
                    alt="logo"
                  />
                </div>
              </Link>
              <div>Безопасность</div>
            </div>
            {/*<div className="services-item"><div>4</div><div>4.2</div></div> */}
          </div>
        </div>
        <div className="margin-bg5"></div>
        <Footer />

        {/* <div>
        <main>{children}</main>
        <footer>
          © {new Date().getFullYear()}, Built with
          {` `}
          <a href="https://www.gatsbyjs.org">Gatsby</a>
        </footer>
      </div>
      */}
      </div>
    </div>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
