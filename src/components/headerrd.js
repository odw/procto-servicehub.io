import React from 'react';
//import React, { useState, useEffect, useRef } from "react";
//import ReactDOM from "react-dom";
import '../styles/menus.css';
//import ResizeDetector from 'react-resize-detector';
import {Link} from 'gatsby';
//import {Route} from 'react-router-dom';

const HeaderRD = () => (
  <div id="menubar" className={'headerrd'}>
    <nav id="menu">
      <ul className={'menu menu-dropdown'}>
        <li className={'level1 parent '}>
          <Link to="/" className={'level1 parent '}>
            <span>Главная</span>
          </Link>

          <div className={'dropdown columns1'}>
            <div className={'dropdown-bg'}>
              <div>
                <div className={'width100 column'}>
                  <ul className={'level2'}>
                    <li className={'level2 hassubtitle'}>
                      <Link
                        to="https://www.youtube.com/channel/UCQ_Wm6TU0aIdnz52bl-Y3Zg"
                        target="_blank"
                        className={'level2'}
                      >
                        <span>
                          <span className={'title'}>Видео</span>
                          <span className={'subtitle'}>Youtube канал</span>
                        </span>
                      </Link>
                    </li>
                    <li className={'level2'}>
                      <Link to="/drag_racing" className={'level2'}>
                        <span>DRAG RACING</span>
                      </Link>
                    </li>
                    <li className={'level2 parent'}>
                      <span className={'level2 parent'}>
                        <span>Наши бренды</span>
                      </span>
                      <ul className={'level3'}>
                        <li className={'level3'}>
                          <Link to="/starline" className={'level3'}>
                            <span>ТМ Starline</span>
                          </Link>
                        </li>
                        {/*
                        <li className={'level3'}>
                          <Link to="#" className={'level3'}>
                            <span>Race CHIP</span>
                          </Link>
                        </li>
                        s*/}
                        <li className={'level3'}>
                          <Link to="/revo" className={'level3'}>
                            <span>REVO</span>
                          </Link>
                        </li>
                        {/*
                        <li className={'level3'}>
                          <Link to="#" className={'level3'}>
                            <span>MOBIL</span>
                          </Link>
                        </li>
                        <li className={'level3'}>
                          <Link to="#" className={'level3'}>
                            <span>YACCO</span>
                          </Link>
                        </li>
                        */}
                      </ul>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </li>
        <li className={'level1 parent'}>
          <span className={'level1 parent'}>
            <span>Детейлинг</span>
          </span>
          <div className={'dropdown columns1'}>
            <div className={'dropdown-bg'}>
              <div>
                <div className={'width100 column'}>
                  <ul className={'level2'}>
                    <li className={'level2'}>
                      <Link to="/dry_cleaning" className={'level3'}>
                        <span className={'level3'}>Химчистка</span>
                      </Link>
                    </li>

                    <li className={'level2 parent'}>
                      <span className={'level2 parent'}>
                        <span>Полировка</span>
                      </span>
                      <ul className={'level3'}>
                        <li className={'level3'}>
                          <Link to="/polish_car_body" className={'level3'}>
                            <span>Полировка кузова</span>
                          </Link>
                        </li>
                        <li className={'level3'}>
                          <Link
                            to="/polish_lights"
                            title="Полировка фар и фонарей на авто"
                            className={'level3'}
                          >
                            <span>Полировка фар</span>
                          </Link>
                        </li>
                      </ul>
                    </li>
                    <li className={'level2'}>
                      <span className={'level2'}>
                        <Link to="/local_painting" className={'level3'}>
                          <span>Локальная покраска</span>
                        </Link>
                      </span>
                    </li>
                    {/*
                  <li className={"level2"}>
                    <Link to="#" className={"level2"}>
                      <span>Классификация авто</span>
                    </Link>
                    </li> */}
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </li>

        {/*
      <li className={"level1 parent"}>
        <Link to="#" 
          title="Оклейка автомобилей виниловыми плёнками"
          
          className={"level1 parent"}
        >
          <span>Винил</span>
        </Link>
        <div className={"dropdown columns1"}>
          <div className={"dropdown-bg"}>
            <div>
              <div className={"width100 column"}>
                <ul className={"level2"}>
                  <li className={"level2"}>
                    <Link to="#" className={"level2"}>
                      <span>Полная оклейка кузова</span>
                    </Link>
                  </li>
                  <li className={"level2"}>
                    <Link to="#" className={"level2"}>
                      <span>Салон</span>
                    </Link>
                  </li>
                
                  <li className={"level2"}>
                    <Link to="#"
                      title="Бронирование прозрачной плёнкой"
                      
                      className={"level2"}
                    >
                      <span>Антигравийная защита кузова</span>
                    </Link>
                  </li>
                  <li className={"level2"}>
                    <Link to="#" className={"level2"}>
                      <span>Бронирование оптики</span>
                    </Link>
                  </li>
                  <li className={"level2"}>
                    <Link to="#" className={"level2"}>
                      <span>Тонировка оптики</span>
                    </Link>
                  </li>
                  <li className={"level2"}>
                    <Link to="#" className={"level2"}>
                      <span>Тонировка</span>
                    </Link>
                  </li>                   
                  
                </ul>
              </div>
            </div>
          </div>
        </div>
      </li>
        */}
        <li className={'level1 parent'}>
          <span className={'level1 parent'}>
            <span>Автосервис</span>
          </span>
          <div className={'dropdown columns2'}>
            <div className={'dropdown-bg'}>
              <div>
                <div className={'width50 column'}>
                  <ul className={'level2'}>
                    <li className={'level2 parent hassubtitle'}>
                      <span className={'level2 parent'}>
                        <span
                          className={'icon'}
                          style={{
                            backgroundImage:
                              'url(' + require('../images/dtp.png') + ')',
                          }}
                        ></span>
                        <span className={'title'}>Кузовной ремонт</span>
                        <span className={'subtitle'}>Покраска авто и мото</span>
                      </span>

                      <ul className={'level3'}>
                        <li className={'level3'}>
                          <Link
                            to="/car_painting"
                            title="Профессиональная покраска автомобиля"
                            className={'level3'}
                          >
                            <span>Покраска автомобилей</span>
                          </Link>
                        </li>
                        <li className={'level3'}>
                          <Link
                            to="/local_repair"
                            title="Локальная покраска и удаление вмятин"
                            className={'level3'}
                          >
                            <span>Локальный ремонт</span>
                          </Link>
                        </li>
                        <li className={'level3'}>
                          <Link to="/bumper_repair" className={'level3'}>
                            <span>Ремонт бамперов</span>
                          </Link>
                        </li>
                        <li className={'level3'}>
                          <Link
                            to="/car_disks_painting"
                            title="Покраска колёсных дисков порошковой краской"
                            className={'level3'}
                          >
                            <span>Покраска дисков автомобиля</span>
                          </Link>
                        </li>
                      </ul>
                    </li>

                    <li className={'level2 parent'}>
                      <span className={'level2 parent'}>
                        <span>
                          <span
                            className={'icon'}
                            style={{
                              backgroundImage:
                                'url(' + require('../images/window.png') + ')',
                            }}
                          >
                            {' '}
                          </span>
                          Стёкла
                        </span>
                      </span>
                      <ul className={'level3'}>
                        <li className={'level3'}>
                          <Link
                            to="/tone_windows"
                            title="Тонировка стёкол автомобиля плёнками LLUMAR и 3М"
                            className={'level3'}
                          >
                            <span>Тонировка стекол</span>
                          </Link>
                        </li>
                        <li className={'level3'}>
                          <span className={'level3'}>
                            <Link
                              to="/front_glass_replacement"
                              title="Тонировка стёкол автомобиля плёнками LLUMAR и 3М"
                              className={'level3'}
                            >
                              <span>Замена лобового стекла</span>
                            </Link>
                          </span>
                        </li>
                      </ul>
                    </li>
                    <li className={'level2 parent'}>
                      <span className={'level2 parent'}>
                        <span>
                          <span
                            className={'icon'}
                            style={{
                              backgroundImage:
                                'url(' + require('../images/carbon.png') + ')',
                            }}
                          >
                            {' '}
                          </span>
                          Тюнинг
                        </span>
                      </span>
                      <ul className={'level3'}>
                        <li className={'level3'}>
                          <Link
                            to="/carbon"
                            title="Декорирование деталей карбоном (углеткань)"
                            className={'level3'}
                          >
                            <span>Карбон | Углеткань</span>
                          </Link>
                        </li>
                        <li className={'level3'}>
                          <Link to="/exhaust" className={'level3'}>
                            <span>Выхлопные системы</span>
                          </Link>
                        </li>
                        <li className={'level3'}>
                          <Link to="/car_body_tuning" className={'level3'}>
                            <span>Кузов</span>
                          </Link>
                        </li>
                        <li className={'level3'}>
                          <Link to="/intake_tuning" className={'level3'}>
                            <span>Впуск / выпуск</span>
                          </Link>
                        </li>
                        <li className={'level3'}>
                          <Link to="/noise_isolation" className={'level3'}>
                            <span>Шумоизоляция</span>
                          </Link>
                        </li>
                        <li className={'level3'}>
                          <Link to="/chip_tuning" className={'level3'}>
                            <span>Чип тьюнинг</span>
                          </Link>
                        </li>
                        <li className={'level3'}>
                          <Link to="/salon_tuning" className={'level3'}>
                            <span>Салон</span>
                          </Link>
                        </li>
                        <li className={'level3'}>
                          <Link to="/lights_tuning" className={'level3'}>
                            <span>Тьюнинг оптики</span>
                          </Link>
                        </li>
                      </ul>
                    </li>
                    {/*
                    <li className={'level2 parent'}>
                      <Link
                        to="#"
                        title="Шиномонтаж stance"
                        className={'level2 parent'}
                      >
                        <span>
                          <span
                            className={'icon'}
                            style={{
                              backgroundImage:
                                'url(' + require('../images/wheel.png') + ')',
                            }}
                          >
                            {' '}
                          </span>
                          Шиномонтаж
                        </span>
                      </Link>
                    </li> */}
                  </ul>
                </div>
                <div className={'width50 column'}>
                  <ul className={'level2'}>
                    <li className={'level2 parent'}>
                      <Link to="#" className={'level2 parent'}>
                        <span>
                          <span
                            className={'icon'}
                            style={{
                              backgroundImage:
                                'url(' +
                                require('../images/icon_wordpress.png') +
                                ')',
                            }}
                          >
                            {' '}
                          </span>
                          Слесарный ремонт
                        </span>
                      </Link>
                      <ul className={'level3'}>
                        <li className={'level3'}>
                          <Link to="/maintenance" className={'level3'}>
                            <span>Техническое обслуживание</span>
                          </Link>
                        </li>
                        <li className={'level3'}>
                          <Link
                            to="/diag"
                            title="Диагностика ходовой и двигателя"
                            className={'level3'}
                          >
                            <span>Диагностика</span>
                          </Link>
                        </li>
                        <li className={'level3'}>
                          <Link to="/chassis_repair" className={'level3'}>
                            <span>Ремонт ходовой</span>
                          </Link>
                        </li>
                        <li className={'level3'}>
                          <Link to="/engine_repair" className={'level3'}>
                            <span>Ремонт и обслуживание двигателя</span>
                          </Link>
                        </li>
                        <li className={'level3'}>
                          <Link to="/muffler_repair" className={'level3'}>
                            <span>Ремонт глушителя</span>
                          </Link>
                        </li>
                        <li className={'level3'}>
                          <Link to="/fuel_system_repair" className={'level3'}>
                            <span>Ремонт и обслуживание топливной системы</span>
                          </Link>
                        </li>
                      </ul>
                    </li>

                    <li className={'level2 parent'}>
                      <span>Салон</span>

                      <ul className={'level3'}>
                        <li className={'level3'}>
                          <Link to="/salon" className={'level3'}>
                            <span>Салон</span>
                          </Link>
                        </li>
                        <li className={'level3'}>
                          <Link to="/airbag" className={'level3'}>
                            <span>Подушки безопасности</span>
                          </Link>
                        </li>
                      </ul>
                    </li>

                    {/*
                  <li className={"level2 parent"}>
                    <span className={"level2 parent"}>
                      <span>
                        <span
                          className={"icon"}
                          style={{
                            backgroundImage:
                              "url(" +
                              require("../images/salon.png") +
                              ")"
                          }}
                        >
                          {" "}
                        </span>
                        Пошив салона
                      </span>
                    </span>
                    <ul className={"level3"}>
                      <li className={"level3"}>
                        <span className={"level3"}>
                          <span>Перетяжка руля</span>
                        </span>
                      </li>
                      <li className={"level3"}>
                        <span className={"level3"}>
                          <span>Перешив салона</span>
                        </span>
                      </li>
                      <li className={"level3"}>
                        <span className={"level3"}>
                          <span>Ремонт кожи</span>
                        </span>
                      </li>
                    </ul>
                  </li>
                        */}
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </li>

        <li className={'level1 parent hassubtitle'}>
          <Link to="#" className={'level1 parent'}>
            <span>
              <span className={'title'}>Работы</span>
              <span className={'subtitle'}>Фото отчёт</span>
            </span>
          </Link>
          <div className={'dropdown columns2'}>
            <div className={'dropdown-bg'}>
              <div>
                <div className={'width50 column'}>
                  <ul className={'level2'}>
                    {/*} <li className={"level2"}>
                      <Link to="#" className={"level2"}>
                        <span>Автоспорт</span>
                      </Link>
                    </li>
                      */}
                    <li className={'level2'}>
                      <Link to="/audi" className={'level2'}>
                        <span>Audi</span>
                      </Link>
                    </li>
                    <li className={'level2'}>
                      <Link to="/bentley" className={'level2'}>
                        <span>Bentley</span>
                      </Link>
                    </li>
                    <li className={'level2'}>
                      <Link to="/bmw" className={'level2'}>
                        <span>BMW</span>
                      </Link>
                    </li>
                    <li className={'level2'}>
                      <Link to="/chevrolet" className={'level2'}>
                        <span>Chevrolet</span>
                      </Link>
                    </li>

                    <li className={'level2'}>
                      <Link to="/ford" className={'level2'}>
                        <span>Ford</span>
                      </Link>
                    </li>
                    <li className={'level2'}>
                      <Link to="/honda" className={'level2'}>
                        <span>Honda</span>
                      </Link>
                    </li>
                    <li className={'level2'}>
                      <Link to="/hyundai" className={'level2'}>
                        <span>Hyundai</span>
                      </Link>
                    </li>

                    <li className={'level2'}>
                      <Link to="/jeep" className={'level2'}>
                        <span>Jeep</span>
                      </Link>
                    </li>
                  </ul>
                </div>
                <div className={'width50 column'}>
                  <ul className={'level2'}>
                    <li className={'level2'}>
                      <Link to="/mercedes" className={'level2'}>
                        <span>Mercedes</span>
                      </Link>
                    </li>

                    <li className={'level2'}>
                      <Link to="/nissan" className={'level2'}>
                        <span>Nissan</span>
                      </Link>
                    </li>

                    <li className={'level2'}>
                      <Link to="/porsche" className={'level2'}>
                        <span>Porsche</span>
                      </Link>
                    </li>
                    <li className={'level2'}>
                      <Link to="/range_rover" className={'level2'}>
                        <span>RANGE ROVER</span>
                      </Link>
                    </li>

                    <li className={'level2'}>
                      <Link to="/seat" className={'level2'}>
                        <span>Seat</span>
                      </Link>
                    </li>
                    <li className={'level2'}>
                      <Link to="/subaru" className={'level2'}>
                        <span>Subaru</span>
                      </Link>
                    </li>
                    <li className={'level2'}>
                      <Link to="/toyota" className={'level2'}>
                        <span>Toyota</span>
                      </Link>
                    </li>
                    <li className={'level2'}>
                      <Link to="/volkswagen" className={'level2'}>
                        <span>Volkswagen</span>
                      </Link>
                    </li>

                    <li className={'level2'}>
                      <Link to="/other_transport" className={'level2'}>
                        <span>Иной транспорт</span>
                      </Link>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </li>
        {/*<li className={"level1 parent"}>
        <Link to="#" target="_blank"  className={"level1 parent"}>
          <span>Магазин</span>
        </Link>
        
      </li>
                         */}
        <li className={'level1'}>
          <Link to="/contacts" className={'level1'}>
            <span>Контакты</span>
          </Link>
        </li>
      </ul>
    </nav>
  </div>
);

export default HeaderRD;
