import React, {Component} from 'react';
import {Link} from 'gatsby';
//import { useState } from 'react';
//const axios = require('axios').default;
import axios from 'axios';

class Footer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      number: '',
    };

    this.updateInput = this.updateInput.bind(this);
    this.addNumber = this.addNumber.bind(this);
  }
  updateInput(event) {
    this.setState({number: event.target.value});
  }

  addNumber() {
    axios
      .post(`http://localhost:8086/number?number=${this.state.number}`, {
        number: '555',
      })
      .then((res) => {
        console.log(res);
        console.log(res.data);
      });
    /*axios.post('/number', {
            number: '444',
            
        });
        
       axios.post('/number', {
        number: '333333',
        
    });*/
    /*
        axios.post('/number?number=8977', )
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      })
      */
  }

  // const [input, setInput] = useState('');
  // const input
  render() {
    return (
      <div className="footer" id="footer">
        <div style={{display: 'flex', flexDirection: 'row'}}>
          <div className={'footer'}>
            <div>
              <a className={'button-blue-border'} href="tel:+380633069848">
                {/*<img src={require('../images/phone-24px.svg')} alt="logo" />*/}
                <div
                  style={{
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'center',
                  }}
                >
                  <span style={{margin: '10px 20px 0 20px'}}>
                    {' '}
                    +380633069848
                  </span>
                  <span
                    className={'button-blue-border'}
                    style={{margin: '10px 10px', textAlign: 'center'}}
                  >
                    {' '}
                    ПОЗВОНИТЬ
                  </span>
                </div>
              </a>
              <a className={'button-blue-border'} href="#">
                <img src={require('../images/telegram_logo.svg')} alt="logo" />
                <span>TELEGRAM </span>
              </a>
              <div>
                <a
                  className={'button-blue-border'}
                  href="viber://add?number=380633069848"
                >
                  <img src={require('../images/viber_logo.svg')} alt="logo" />
                  <span>VIBER</span>
                </a>
              </div>
              <a
                id="logo"
                className={'button-blue-border'}
                href="https://www.instagram.com/procto_service/"
                target="_blank"
              >
                <img
                  src={require('../images/instagram_logo_2016.svg')}
                  alt="logo"
                />
                <span>INSTAGRAM</span>
              </a>
              {/*<div className={'button-blue-border'} style={{borderRadius:'3px'}}>
      <div style={{display:'flex',flexDirection:'column',justifyContent:'center'}}>
    <input type="text" onChange={this.updateInput} value={this.state.number} maxlength="13" size="13" className={'button-blue-border'} style={{marginBottom:'4px'}}></input>
    <span onClick={this.addNumber} className={'button-blue-border'} style={{textAlign:'center'}}>ПЕРЕЗВОНИТЕ</span>
    </div>
    */}
              {/*<img src={require('../images/phone-24px.svg')} alt="logo"  /> */}
              {/*</div>*/}
              <div>
                <a
                  className="button-blue-border"
                  href="https://goo.gl/maps/sBQLXwh3Qhi8h2ar5"
                  target="_blank"
                >
                  <img src={require('../images/location.svg')} alt="logo" />
                  <div
                    className="-button-blue-border"
                    style={{display: 'block', margin: '10px 10px'}}
                  >
                    GOOGLE КАРТЫ
                  </div>
                </a>
              </div>
            </div>
          </div>
          <a
            id="logo"
            href="/"
            style={{
              display: 'flex',
              width: '100%',
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              margin: '20px 10px',
            }}
          >
            <img
              style={{width: '85%', height: 'auto'}}
              src={require('../images/logo_white_letter.svg')}
              alt="logo"
              width="auto"
            />
          </a>
        </div>
        <h4 style={{alignSelf: 'center'}}>
          Copyright PROCTO SERVICE - Лучшее тьюнинг СТО в Одессе{' '}
        </h4>
        <div className="contacts-fixed">
          <Link style={{zIndex: '100', display: 'block'}} to="#footer">
            <img
              src={require('../images/phone-24px.svg')}
              style={{width: '56px', height: '56px'}}
              alt="logo"
            />
          </Link>
        </div>
        <img
          className="ring"
          src={require('../images/phone_call_circle_border_24px.svg')}
          style={{
            position: 'fixed',
            bottom: '10px',
            right: '10px',
            width: '56px',
            height: '56px',
          }}
          alt="logo"
        />
      </div>
    );
  }
}
export default Footer;
