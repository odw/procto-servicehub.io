import React from "react"
//import Header from '../components/header'
import Footer from "../components/footer"
import HeaderL from "../components/headerl"
import HeaderRUp from "../components/headerrup"
import HeaderRD from "../components/headerrd"
import Post from "./post"
import Posts from "./posts"
const Porsche = () => {
  return (
    <div className="container">
      <div className="wrapper">
        <HeaderL />
        <HeaderRUp />
        <HeaderRD />
        <div className="main-home main-article">
          <Posts
            posts={[
              510,
              490,
              472,
              468,
              467,
              453,
              438,
              429,
              392,
              371,
              354,
              338,
              334,
              306,
            ]}
          />
        </div>
        <Footer />
      </div>
    </div>
  )
}
export default Porsche
