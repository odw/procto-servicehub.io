import React from "react"
//import Header from '../components/header'
import Footer from "../components/footer"
import HeaderL from "../components/headerl"
import HeaderRUp from "../components/headerrup"
import HeaderRD from "../components/headerrd"
import Post from "./post"
import Posts from "./posts"
const Audi = () => {
  return (
    <div className="container">
      <div className="wrapper">
        <HeaderL />
        <HeaderRUp />
        <HeaderRD />
        <div className="main-home main-article">
          <Posts
            posts={[
              539,
              538,
              537,
              534,
              527,
              524,
              523,
              518,
              517,
              501,
              487,
              479,
              466,
              464,
              462,
              461,
              460,
              455,
              450,
              433,
              420,
              419,
              416,
              415,
              414,
              383,
              382,
              381,
              380,
              372,
              357,
              355,
              350,
              312,
              300,
              275,
              256,
              252,
              245,
              232,
              228,
              191,
              186,
              172,
              140,
              136,
              129,
              115,
              114,
              88,
              84,
              46,
              45,
              44,
              37,
              36,
              35,
              34,
              28,
              27,
              26,
              24,
              23,
              14,
              13,
              12,
              8,
            ]}
          />
        </div>
        <Footer />
      </div>
    </div>
  )
}
export default Audi
