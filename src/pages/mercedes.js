import React from "react"
//import Header from '../components/header'
import Footer from "../components/footer"
import HeaderL from "../components/headerl"
import HeaderRUp from "../components/headerrup"
import HeaderRD from "../components/headerrd"
import Post from "./post"
import Posts from "./posts"
const Mercedes = () => {
  return (
    <div className="container">
      <div className="wrapper">
        <HeaderL />
        <HeaderRUp />
        <HeaderRD />
        <div className="main-home main-article">
          <Posts
            posts={[
              499,
              494,
              480,
              477,
              475,
              465,
              444,
              440,
              434,
              432,
              404,
              401,
              394,
              361,
              360,
              359,
              358,
              356,
              352,
              314,
              308,
              304,
              289,
              286,
              281,
              243,
              227,
              226,
              225,
              222,
              169,
              152,
              147,
              146,
              142,
              139,
              89,
              82,
              81,
              80,
              51,
              40,
              25,
              22,
              21,
              15,
              7,
              6,
              5,
            ]}
          />
        </div>
        <Footer />
      </div>
    </div>
  )
}
export default Mercedes
