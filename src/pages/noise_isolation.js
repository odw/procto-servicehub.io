import React from 'react';
//import Header from '../components/header'
import Footer from '../components/footer';
import HeaderL from '../components/headerl';
import HeaderRUp from '../components/headerrup';
import HeaderRD from '../components/headerrd';
import Post from './post';
import Posts from './posts';
const Noise_isolation = () => {
  return (
    <div className="container">
      <div className="wrapper">
        <HeaderL />
        <HeaderRUp />
        <HeaderRD />
        <div className="main-home main-article">
          <div className="button-blue-border">
            <div className="button-blue-border">
              <img src="/static/images/noise_isolation.jpg"></img>
            </div>
            <div className="button-blue-border" style={{padding: '20px'}}>
              У нас доступна услуга по шумоизоляции автомобиля. Наиболее часто у
              нас ее заказывают после установки выхлопа.
            </div>
          </div>
          <Posts posts={[227]} />
        </div>
        <Footer />
      </div>
    </div>
  );
};
export default Noise_isolation;
