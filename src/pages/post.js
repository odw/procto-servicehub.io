import React from 'react';
//import Header from '../components/header'
import posts_data from '../content/posts_data.json';
const Post = (props) => {
  //console.log(props.id)

  let id = posts_data.posts.filter((post) => post.id === props.id);
  let postType;
  if (id[0]) {
    postType = id[0].mediatype;
  } else postType = '';
  //let postType = id[0].mediatype;
  console.log(postType);
  //return <div></div>

  if (postType === 'video') {
    return (
      <div>
        {id.map((val) => (
          <div key={val.id} className="button-blue-border">
            <div className="button-blue-border">
              <video
                controls
                src={'/static/images/instagram/' + val.id + '.mp4'}
                style={{maxWidth: '100%', width: '100%'}}
              ></video>
            </div>
            <div className="button-blue-border" style={{padding: '20px'}}>
              {val.title}
            </div>
          </div>
        ))}
      </div>
    );
  }
  if (postType === 'youtube') {
    return <div></div>;
  }
  if (postType === 'audio') {
    return <div></div>;
  }
  return (
    <div>
      {id.map((val) => (
        <div key={val.id} className="button-blue-border">
          <div className="button-blue-border">
            <img
              src={'/static/images/instagram/' + val.id + '.jpg'}
              style={{maxWidth: '100%'}}
            ></img>
          </div>
          <div className="button-blue-border" style={{padding: '20px'}}>
            {val.title}
          </div>
        </div>
      ))}
    </div>
  );
};
export default Post;
