import React from "react"
//import Header from '../components/header'
import Footer from "../components/footer"
import HeaderL from "../components/headerl"
import HeaderRUp from "../components/headerrup"
import HeaderRD from "../components/headerrd"
import Post from "./post"
import Posts from "./posts"
const Car_disks_painting = () => {
  return (
    <div className="container">
      <div className="wrapper">
        <HeaderL />
        <HeaderRUp />
        <HeaderRD />
        <div className="main-home main-article">
          <Posts
            posts={[
              531,
              510,
              463,
              460,
              459,
              412,
              406,
              395,
              264,
              259,
              225,
              171,
              117,
              105,
              104,
              103,
              102,
              101,
              32,
            ]}
          />
        </div>
        <Footer />
      </div>
    </div>
  )
}
export default Car_disks_painting
