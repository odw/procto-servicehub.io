import React from "react"
//import Header from '../components/header'
import Footer from "../components/footer"
import HeaderL from "../components/headerl"
import HeaderRUp from "../components/headerrup"
import HeaderRD from "../components/headerrd"
import Post from "./post"
import Posts from "./posts"
const Tone_windows = () => {
  return (
    <div className="container">
      <div className="wrapper">
        <HeaderL />
        <HeaderRUp />
        <HeaderRD />
        <div className="main-home main-article">
          <Posts posts={[506, 494, 493, 416, 374, 336, 236, 52, 25]} />
        </div>
        <Footer />
      </div>
    </div>
  )
}
export default Tone_windows
