import React from 'react';
//import Header from '../components/header'
import Post from './post';

const Posts = (props) => {
  //console.log(props.id)
  //const id = posts.filter(post => post.id === props.id)
  //return <div></div>
  let posts;
  if (props.posts) posts = props.posts.map((val) => <Post id={val} />);
  else posts = '';

  console.log(posts);
  return posts;
};
export default Posts;
