import React from 'react';
//import Header from '../components/header'
import Footer from '../components/footer';
import HeaderL from '../components/headerl';
import HeaderRUp from '../components/headerrup';
import HeaderRD from '../components/headerrd';
import Post from './post';
import Posts from './posts';
const Usa_car = () => {
  return (
    <div className="container">
      <div className="wrapper">
        <HeaderL />
        <HeaderRUp />
        <HeaderRD />
        <div className="main-home main-article">
          <p>
            Наше СТО доставило уже более 150 авто из США. Вы можете съэкономить
            значительную сумму при покупке авто из США.
          </p>
          <Posts posts={[533, 526]} />
        </div>
        <Footer />
      </div>
    </div>
  );
};
export default Usa_car;
