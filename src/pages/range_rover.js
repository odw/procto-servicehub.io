import React from "react"
//import Header from '../components/header'
import Footer from "../components/footer"
import HeaderL from "../components/headerl"
import HeaderRUp from "../components/headerrup"
import HeaderRD from "../components/headerrd"
import Post from "./post"
import Posts from "./posts"
const Range_rover = () => {
  return (
    <div className="container">
      <div className="wrapper">
        <HeaderL />
        <HeaderRUp />
        <HeaderRD />
        <div className="main-home main-article">
          <Posts
            posts={[
              514,
              493,
              456,
              452,
              437,
              435,
              408,
              403,
              376,
              332,
              325,
              297,
              296,
              267,
              255,
              244,
              217,
              183,
              178,
              171,
              144,
              72,
              29,
            ]}
          />
        </div>
        <Footer />
      </div>
    </div>
  )
}
export default Range_rover
