import React from "react"
//import Header from '../components/header'
import Footer from "../components/footer"
import HeaderL from "../components/headerl"
import HeaderRUp from "../components/headerrup"
import HeaderRD from "../components/headerrd"
import Post from "./post"
import Posts from "./posts"
const Nissan = () => {
  return (
    <div className="container">
      <div className="wrapper">
        <HeaderL />
        <HeaderRUp />
        <HeaderRD />
        <div className="main-home main-article">
          <Posts
            posts={[
              428,
              427,
              302,
              301,
              287,
              253,
              242,
              241,
              239,
              178,
              177,
              176,
              170,
              62,
            ]}
          />
        </div>
        <Footer />
      </div>
    </div>
  )
}
export default Nissan
