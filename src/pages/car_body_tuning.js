import React from 'react';
//import Header from '../components/header'
import Footer from '../components/footer';
import HeaderL from '../components/headerl';
import HeaderRUp from '../components/headerrup';
import HeaderRD from '../components/headerrd';
import Post from './post';
import Posts from './posts';
const Car_body_tuning = () => {
  return (
    <div className="container">
      <div className="wrapper">
        <HeaderL />
        <HeaderRUp />
        <HeaderRD />
        <div className="main-home main-article">
          <Posts
            posts={[
              527,
              501,
              461,
              414,
              361,
              360,
              317,
              316,
              310,
              290,
              287,
              281,
              277,
              267,
              243,
              178,
              177,
              172,
              154,
              128,
              95,
              91,
              89,
              50,
              49,
              46,
              45,
              44,
              34,
              12,
            ]}
          />
        </div>
        <Footer />
      </div>
    </div>
  );
};
export default Car_body_tuning;
