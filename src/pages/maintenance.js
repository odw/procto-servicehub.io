import React from "react"
//import Header from '../components/header'
import Footer from "../components/footer"
import HeaderL from "../components/headerl"
import HeaderRUp from "../components/headerrup"
import HeaderRD from "../components/headerrd"
import Post from "./post"
import Posts from "./posts"
const Maintenance = () => {
  return (
    <div className="container">
      <div className="wrapper">
        <HeaderL />
        <HeaderRUp />
        <HeaderRD />
        <div className="main-home main-article">
          <Posts
            posts={[
              529,
              521,
              499,
              492,
              480,
              476,
              475,
              457,
              452,
              446,
              394,
              385,
              384,
              377,
              376,
              372,
              366,
              353,
              326,
              313,
              311,
              301,
              273,
            ]}
          />
        </div>
        <Footer />
      </div>
    </div>
  )
}
export default Maintenance
