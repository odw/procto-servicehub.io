import React from "react"
//import Header from '../components/header'
import Footer from "../components/footer"
import HeaderL from "../components/headerl"
import HeaderRUp from "../components/headerrup"
import HeaderRD from "../components/headerrd"
import Post from "./post"
import Posts from "./posts"
const Intake_tuning = () => {
  return (
    <div className="container">
      <div className="wrapper">
        <HeaderL />
        <HeaderRUp />
        <HeaderRD />
        <div className="main-home main-article">
          <Posts posts={[468, 422, 96, 57, 56, 53, 40, 39, 30]} />
        </div>
        <Footer />
      </div>
    </div>
  )
}
export default Intake_tuning
