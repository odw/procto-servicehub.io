import React from "react"
//import Header from '../components/header'
import Footer from "../components/footer"
import HeaderL from "../components/headerl"
import HeaderRUp from "../components/headerrup"
import HeaderRD from "../components/headerrd"
import Post from "./post"
import Posts from "./posts"
const Bmw = () => {
  return (
    <div className="container">
      <div className="wrapper">
        <HeaderL />
        <HeaderRUp />
        <HeaderRD />
        <div className="main-home main-article">
          <Posts
            posts={[
              522,
              521,
              513,
              506,
              482,
              474,
              463,
              449,
              448,
              402,
              397,
              389,
              384,
              369,
              368,
              367,
              364,
              363,
              362,
              361,
              351,
              349,
              348,
              344,
              342,
              339,
              330,
              329,
              282,
              279,
              277,
              276,
              273,
              266,
              251,
              249,
              230,
              229,
              224,
              141,
              138,
              132,
              130,
              116,
              108,
              107,
              98,
              97,
              96,
              93,
              71,
              70,
              69,
              67,
              64,
              61,
              60,
              59,
              48,
              43,
              42,
              31,
              30,
              10,
              1,
              0,
            ]}
          />
        </div>
        <Footer />
      </div>
    </div>
  )
}
export default Bmw
