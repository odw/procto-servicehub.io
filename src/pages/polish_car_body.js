import React from "react"
//import Header from '../components/header'
import Footer from "../components/footer"
import HeaderL from "../components/headerl"
import HeaderRUp from "../components/headerrup"
import HeaderRD from "../components/headerrd"
import Post from "./post"
import Posts from "./posts"
const Polish_car_body = () => {
  return (
    <div className="container">
      <div className="wrapper">
        <HeaderL />
        <HeaderRUp />
        <HeaderRD />
        <div className="main-home main-article">
          <Posts posts={[453, 370, 346, 305, 302, 138, 92, 86, 85, , 72]} />
        </div>
        <Footer />
      </div>
    </div>
  )
}
export default Polish_car_body
