import React from 'react';
//import Header from '../components/header'
import Footer from '../components/footer';
import HeaderL from '../components/headerl';
import HeaderRUp from '../components/headerrup';
import HeaderRD from '../components/headerrd';
import Post from './post';
import Posts from './posts';
const Chip_tuning = () => {
  return (
    <div className="container">
      <div className="wrapper">
        <HeaderL />
        <HeaderRUp />
        <HeaderRD />
        <div className="main-home main-article">
          <Posts
            posts={[
              534,
              518,
              511,
              497,
              472,
              469,
              439,
              433,
              423,
              419,
              413,
              409,
              386,
              383,
              382,
              362,
              357,
              355,
              341,
              265,
              254,
              232,
              184,
              144,
              143,
              142,
              141,
              140,
              131,
              129,
              126,
              125,
              111,
              3,
              2,
              1,
              0,
            ]}
          />
        </div>
        <Footer />
      </div>
    </div>
  );
};
export default Chip_tuning;
