import React from "react"
//import Header from '../components/header'
import Footer from "../components/footer"
import HeaderL from "../components/headerl"
import HeaderRUp from "../components/headerrup"
import HeaderRD from "../components/headerrd"
import Post from "./post"
import Posts from "./posts"
const Chassis_repair = () => {
  return (
    <div className="container">
      <div className="wrapper">
        <HeaderL />
        <HeaderRUp />
        <HeaderRD />
        <div className="main-home main-article">
          <Posts posts={[525, 397, 363, 343, 106, 93, 61, 59, 55, 42]} />
        </div>
        <Footer />
      </div>
    </div>
  )
}
export default Chassis_repair
