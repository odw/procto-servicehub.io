import React from 'react';
//import Header from '../components/header'
import Footer from '../components/footer';
import HeaderL from '../components/headerl';
import HeaderRUp from '../components/headerrup';
import HeaderRD from '../components/headerrd';
import Post from './post';
import Posts from './posts';
const Starline = () => {
  return (
    <div className="container">
      <div className="wrapper">
        <HeaderL />
        <HeaderRUp />
        <HeaderRD />
        <div className="main-home main-article">
          <div className="button-blue-border">
            <img
              src={'/static/images/starline.png'}
              style={{maxWidth: '100%'}}
            ></img>
          </div>
          <div className="button-blue-border" style={{padding: '20px'}}>
            Мы являемся сертифицированной точкой продажи и установки
            сигнализации для авто &quot;TM Starline&quot;
          </div>

          <Posts posts={[]} />
        </div>
        <Footer />
      </div>
    </div>
  );
};
export default Starline;
