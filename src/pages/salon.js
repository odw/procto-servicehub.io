import React from "react"
//import Header from '../components/header'
import Footer from "../components/footer"
import HeaderL from "../components/headerl"
import HeaderRUp from "../components/headerrup"
import HeaderRD from "../components/headerrd"
import Post from "./post"
import Posts from "./posts"
const Salon = () => {
  return (
    <div className="container">
      <div className="wrapper">
        <HeaderL />
        <HeaderRUp />
        <HeaderRD />
        <div className="main-home main-article">
          <Posts posts={[536, 388, 48, 22, 21]} />
        </div>
        <Footer />
      </div>
    </div>
  )
}
export default Salon
