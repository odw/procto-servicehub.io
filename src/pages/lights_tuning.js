import React from 'react';
//import Header from '../components/header'
import Footer from '../components/footer';
import HeaderL from '../components/headerl';
import HeaderRUp from '../components/headerrup';
import HeaderRD from '../components/headerrd';
import Post from './post';
import Posts from './posts';
const Lights_tuning = () => {
  return (
    <div className="container">
      <div className="wrapper">
        <HeaderL />
        <HeaderRUp />
        <HeaderRD />
        <div className="main-home main-article">
          <Posts
            posts={[
              532,
              454,
              404,
              393,
              391,
              368,
              364,
              352,
              344,
              289,
              269,
              147,
              146,
              7,
              6,
            ]}
          />
        </div>
        <Footer />
      </div>
    </div>
  );
};
export default Lights_tuning;
