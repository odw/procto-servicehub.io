import React from "react"
//import Header from '../components/header'
import Footer from "../components/footer"
import HeaderL from "../components/headerl"
import HeaderRUp from "../components/headerrup"
import HeaderRD from "../components/headerrd"
import Post from "./post"
import Posts from "./posts"
const Car_painting = () => {
  return (
    <div className="container">
      <div className="wrapper">
        <HeaderL />
        <HeaderRUp />
        <HeaderRD />
        <div className="main-home main-article">
          <Posts
            posts={[
              483,
              478,
              444,
              429,
              427,
              417,
              408,
              378,
              332,
              325,
              262,
              260,
              242,
              241,
              239,
              217,
              20,
              19,
              5,
            ]}
          />
        </div>
        <Footer />
      </div>
    </div>
  )
}
export default Car_painting
