import React from 'react';
//import Header from '../components/header'
import Footer from '../components/footer';
import HeaderL from '../components/headerl';
import HeaderRUp from '../components/headerrup';
import HeaderRD from '../components/headerrd';
import Post from './post';
import Posts from './posts';
const Drag_racing = () => {
  return (
    <div className="container">
      <div className="wrapper">
        <HeaderL />
        <HeaderRUp />
        <HeaderRD />
        <div className="main-home main-article">
          <Posts posts={[540]} />
          <div className="button-blue-border">
            <div className="button-blue-border">
              <img
                src={'/static/images/award2.png'}
                style={{maxWidth: '100%'}}
              ></img>
            </div>
            <div className="button-blue-border">
              <img
                src={'/static/images/award3.png'}
                style={{maxWidth: '100%'}}
              ></img>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    </div>
  );
};
export default Drag_racing;
