import React from "react"
//import Header from '../components/header'
import Footer from "../components/footer"
import HeaderL from "../components/headerl"
import HeaderRUp from "../components/headerrup"
import HeaderRD from "../components/headerrd"
import Post from "./post"
import Posts from "./posts"
const Jeep = () => {
  return (
    <div className="container">
      <div className="wrapper">
        <HeaderL />
        <HeaderRUp />
        <HeaderRD />
        <div className="main-home main-article">
          <Posts
            posts={[
              492,
              481,
              457,
              425,
              411,
              390,
              260,
              236,
              234,
              87,
              74,
              55,
              32,
              17,
              3,
              2,
            ]}
          />
        </div>
        <Footer />
      </div>
    </div>
  )
}
export default Jeep
