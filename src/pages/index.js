import React from 'react';
import {Link} from 'gatsby';

import Layout from '../components/layout';
import Image from '../components/image';
import SEO from '../components/seo';
//import "../styles/index.scss"
import '../styles/index.css';
const IndexPage = () => <Layout></Layout>;

export default IndexPage;
