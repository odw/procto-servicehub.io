import React from 'react';
//import Header from '../components/header'
import Footer from '../components/footer';
import HeaderL from '../components/headerl';
import HeaderRUp from '../components/headerrup';
import HeaderRD from '../components/headerrd';
import Post from './post';
import Posts from './posts';
const Exhaust = () => {
  return (
    <div className="container">
      <div className="wrapper">
        <HeaderL />
        <HeaderRUp />
        <HeaderRD />
        <div className="main-home main-article">
          <Posts
            posts={[
              528,
              522,
              520,
              517,
              507,
              503,
              486,
              484,
              482,
              462,
              455,
              450,
              437,
              436,
              434,
              432,
              426,
              424,
              421,
              420,
              418,
              401,
              396,
              367,
              358,
              354,
              349,
              342,
              341,
              337,
              333,
              320,
              314,
              312,
              306,
              303,
              299,
              297,
              296,
              288,
              286,
              276,
              270,
              266,
              251,
              249,
              222,
              175,
              174,
              155,
              132,
              115,
              113,
              112,
              100,
              99,
              88,
              84,
              82,
              77,
              76,
              71,
              70,
              69,
              51,
              38,
              37,
              36,
              35,
              29,
              18,
              17,
              16,
              15,
              14,
              13,
            ]}
          />
        </div>
        <Footer />
      </div>
    </div>
  );
};
export default Exhaust;
{
  /*Изготовление выхлопных систем любой сложности. Спортивный выхлоп, прямоточный Глушитель, камерный глушитель, резонатор, прямоточный резонатор, Даунпайп 

Изготовление систем по вашему образцу, ремонт выхлопных систем и комплектующих, заслонки, Катализаторы

Опыт работы в этой сфере более 10лет, только качественные материалы и дорогостоящее оборудование. Все вопросы по Телефону либо вайбер, вотсап

Ссылка на наш сайт


https://www.instagram.com/procto_service/
*/
}
